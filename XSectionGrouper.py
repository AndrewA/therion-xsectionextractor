#!/usr/bin/python

##
## Copyright (C) 2013-19 Andrew Atkinson
##
##-------------------------------------------------------------------
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##-------------------------------------------------------------------

import logging
import math
import os
import re
#from kivy.app import App
#from kivy.uix.widget import Widget

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


class SectionSize:
    '''stores the size of a cross section'''
    def __init__(self, name, scale, maxmin):
        self.name = name
        self.scale = scale
        self.x = float(maxmin['max']['x']) - float(maxmin['min']['x'])
        self.y = float(maxmin['max']['y']) - float(maxmin['min']['y'])


class SectionLabel:
    '''stores the labels for cross section'''
    def __init__(self, name, label):
        self.name = name
        self.label = label


def addlabels(sections, labels):
    '''adds the label attibute to the sections'''
    for section in sections:
        logging.debug('Trying to add label to %s', section.name)
        for label in labels:
            logging.debug('searching label %s, %s', label.name, label.label,)
            if section.name == label.name:
                section.label = label.label
                logging.info('Added %s to %s', section.label, section.name)
                break
        try:
            section.label
        except AttributeError:
            logging.info('No label found for %s', section.name)
        except:
            raise
    return


def typescrap(txt, sections, th2file):
    '''Check scrap is projection none, then get name and size'''
    re1 = 'scrap'
    re2 = '\\s+'  # White Space
    re3 = '(.*?)'  # all character  scrapname
    re4 = '\\s+'  # White Space
    re5 = '-proj'  # Word
    re6 = '.*?'  # Non-greedy match on filler
    re7 = 'none'  # Word
    re8 = '.*?'  # Non-greedy match on filler
    re9 = '-scale'  # Word
    re10 = '\\s+'  # White Space
    re11 = '(\\[.*?\\])'  # Square Braces

    rg = re.compile(re1 + re2 + re3 + re4 + re5 + re6 +
                    re7 + re8 + re9 + re10 + re11,
                    re.IGNORECASE | re.DOTALL)
    m = rg.search(txt)
    if m:
        try:
            maxmin = sizescrap(th2file)
        #maxmin is not assigned if the scrap is not drawn, catch error
        except UnboundLocalError:
            logging.info('Scrap is probably empty %s', m.group(1))
        except:
            raise
        else:
            scrapname = m.group(1)
            scale = m.group(2)
            logging.debug('%s, %s', scrapname, scale)
            sections.append(SectionSize(scrapname, scale, maxmin))
    else:
        logging.debug('scrap not projection none')
    return


def extractlabel(txt, sectionlabels):
    '''In label looks for correct attibution then gets the label text'''
    re1 = '-attr\\s+scrapname\\s+'
    re2 = '(.*?)'  # scrapname
    re3 = '\\s+'  # white space
    rg = re.compile(re1 + re2 + re3, re.IGNORECASE)
    m = rg.search(txt)
    if m:
        scrapname = m.group(1)
        logging.debug('Scrapname:: %s', txt)
        #re2 = '-text\\s+'
        #re3 = '\"(.*?)\"'  # get the text in quotes
        re2 = '-text\s+'
        re3 = '[\[\"]?(.*?)[\]\"]?[\s|$]'  # get the text might have [] or ""
        rg = re.compile(re2 + re3, re.IGNORECASE)
        n = rg.search(txt)
        if n:
            logging.info('Scrapname %s has label %s', scrapname, n.group(1))
            sectionlabels.append(SectionLabel(scrapname, n.group(1)))
        else:
            logging.warning('Attributed label %s has no label text', scrapname)
    else:
        logging.debug('Label with no attibution')
    return


def extractpoint(txt, sectionlabels, fixpoint):
    '''Check if point then get type. If type is
     -label, extract x-section name
     -station: get name for one point only
'''

    re1 = 'point'
    re2 = '.*?'  # none greedy filler
    re3 = '\\s+'  # white space
    re4 = '([a-z]*?)'  # point type
    re5 = '\\s+'  # white space
    re6 = '(.*$)'  # rest of line
    rg = re.compile(re1 + re2 + re3 + re4 + re5 + re6, re.IGNORECASE)
    m = rg.search(txt)
    if m:
        if m.group(1) == 'label':
            extractlabel(m.group(2), sectionlabels)
        elif m.group(1) == 'station' and not fixpoint:
            fixpoint = extractstation(m.group(2))
    return fixpoint


def extractstation(txt):
    '''Finds the name of the station'''
    re1 = '-name\\s+'
    re2 = '(.*?)'  # stationname
    re3 = '(?:$|\\s+)'  # end of line or white space
    #TODO Does not deal with station names in quotes
    rg = re.compile(re1 + re2 + re3, re.IGNORECASE)
    m = rg.search(txt)
    if m:
        fixpoint = m.group(1)
        logging.debug('Station name %s', fixpoint)
    #FIXME Breaks if the station does not have a name
    return fixpoint


def scanfile(root, name, settings):
    '''Extracts the scraps and labels for scraps from the th2 file'''
    fullname = os.path.join(root, name)
    logging.info('Found file %s', fullname)
    with open(fullname, "r") as th2file:
        sections = []
        sectionlabels = []
        fixpoint = ''
        txt = (th2file.readline())
        #close the XSection file at break of loop
        while True:
            re1 = '(.*?)'  # Non-greedy matchon filler
            re2 = '(\\s+)'  # White Space 1

            rg = re.compile(re1 + re2, re.IGNORECASE | re.DOTALL)
            m = rg.search(txt)
            if m:
                if m.group(1) == 'scrap':
                        typescrap(txt, sections, th2file)
                elif m.group(1) == 'point':
                    fixpoint = extractpoint(txt, sectionlabels, fixpoint)
            txt = th2file.readline()
            if not txt:
                addlabels(sections, sectionlabels)
                if sections:
                    writefile(sections, root, name, fixpoint, settings)
                return


def sizescrap(th2file):
    '''Finds the size of a scrap'''
    new = True
    txt = (th2file.readline())
    while txt:
        rg = re.compile('([+-]?\\d*\\.\\d+)', re.IGNORECASE | re.DOTALL)
        coords = rg.findall(txt)
        if coords:
            #logging.debug('%d, %s', len(coords), coords)
            if new:
                maxmin = {'min': {'x': float(coords[0]), 'y': float(coords[1])},
                          'max': {'x': float(coords[0]), 'y': float(coords[1])}}
                new = False
            for x, y in zip(coords, coords[1:])[::2]:
                if float(x) < float(maxmin['min']['x']):
                    maxmin['min']['x'] = x
                elif float(x) > float(maxmin['max']['x']):
                    maxmin['max']['x'] = x
                if float(y) < float(maxmin['min']['y']):
                    maxmin['min']['y'] = y
                elif float(y) > float(maxmin['max']['y']):
                    maxmin['max']['y'] = y
        else:
            rg = re.compile('endscrap', re.IGNORECASE | re.DOTALL)
            endscrap = rg.search(txt)
            if endscrap:
                return maxmin
        txt = th2file.readline()


def writefile(sections, root, name, fixpoint, settings):
    '''Writes the th2 file with sections in'''
    writename = os.path.join(root, 'XSections' + name)
    with open(writename, 'w') as xth2file:
        currentxy = {'x': 0, 'y': 0}
        #TODO automatic area adjust
        header = '''encoding  utf-8
##XTHERION## xth_me_area_adjust 0 0 1600 1200
##XTHERION## xth_me_area_zoom_to 100

'''
        xth2file.writelines(header)
        scount = 1
        ysize = settings['minspacing'][1]
        for section in sections:
            logging.debug('x position: %d', currentxy['x'])
            if currentxy['x'] == 0 and currentxy['y'] == 0:
                startscrap = ('scrap XSectionSP -projection plan -scale {}'.
                                format(section.scale))
                xth2file.writelines(startscrap)
                xth2file.writelines('\n\npoint 0.0 0.0 station -name {}\n\n'.
                                    format(fixpoint))
            if section.x < settings['minspacing'][0]:
                xsize = settings['minspacing'][0]
            else:
                xsize = section.x
            #Calculate x,y positions
            x = (currentxy['x'] + settings['border'][0] +
                                xsize / 2) * math.cos(settings['rotation'])
            y = ((currentxy['y'] + settings['border'][1] +
                                settings['textspace'][1]))
            xth2file.write('point {:.1f} {:.1f} section '
                        '-scrap "{}" -align t\n\n'.
                        format(x * math.cos(settings['rotation']) -
                                y * math.sin(settings['rotation']),
                                x * math.sin(settings['rotation']) +
                                y * math.cos(settings['rotation']),
                                section.name))
            if section.y > ysize:
                ysize = section.y
            #add in a space between the text
            y -= settings['textspace'][1] / 2
            try:
                xth2file.write('point {:.1f} {:.1f} label -text "{}" '
                                '-align c -scale s\n\n'.
                                format(x * math.cos(settings['rotation']) -
                                y * math.sin(settings['rotation']),
                                x * math.sin(settings['rotation']) +
                                y * math.cos(settings['rotation']),
                                        section.label))
            except AttributeError:
                logging.info('%s has no label', section.name)
            except:
                raise
            currentxy['x'] += 2 * settings['border'][0] + xsize
            if scount == maxline:
                currentxy['y'] += 2 * settings['border'][0] + ysize
                scount = 1
        xth2file.writelines('endscrap')


#class XSectionGrouperApp(App):
    #def build(self):
        #return XSectionGUI()


if __name__ == '__main__':

    currentth2 = 'notset'
    maxline = 10
    horizontal = True  # False not supported as yet
    #XSectionGrouperApp().run()
    settings = {}
    settings['border'] = (40.0, 40.0)
    settings['minspacing'] = (400, 400)
    settings['textspace'] = (200.0, 400.0)
    settings['rotation'] = math.radians(-15)

    th2search = re.compile(r'\.th2$', re.IGNORECASE)
    xsectionfile = 'XSection'
    directory = '/media/data/Andrew/Caving/Surveys/Burrington/Reads/'
    for root, dirs, files in os.walk(directory):
        if '.svn' in dirs:
            dirs.remove('.svn')  # don't visit svn directories
        for name in files:
            if th2search.search(name) is not None and xsectionfile not in name:
                scanfile(root, name, settings)